<?php

namespace Http\Controllers;

use App\Models\Nfe;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NfeControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_returns_nfe_value()
    {
        $nfeValue = "123.0";

        Nfe::create(['id' => 1, 'value' => $nfeValue]);

        $response = $this->get('api/nfe/1');

        $response->assertExactJson(['value' => $nfeValue]);
    }

    public function test_show_returns_404_if_access_key_is_not_found()
    {
        $response = $this->get('api/nfe/1');

        $response->assertStatus(404);
    }
}
