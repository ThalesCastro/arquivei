<?php

namespace Console\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class LoadNfesTest extends TestCase
{
    use RefreshDatabase;

    public function test_makes_api_call_and_inserts_response_on_database()
    {
        Http::fake(["https://sandbox-api.arquivei.com.br/v1/nfe/received" => [
            'data' => [
                ['access_key' => '1234567', 'xml' => $this->getExampleNfeXml()]
            ]
        ]]);

        $this->artisan('nfe:load');

        $this->assertDatabaseHas('nfes', ['id' => '1234567']);
        $this->assertDatabaseCount('nfes', 1);
    }

    private function getExampleNfeXml(): string
    {
        return base64_encode('<nfeProc><NFe><infNFe><total><ICMSTot><vNF>1348.00</vNF></ICMSTot></total></infNFe></NFe></nfeProc>');
    }
}
