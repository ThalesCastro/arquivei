<?php

namespace Services;

use App\DataTransferObjects\NfeResponse;
use App\Services\ArquiveiApiService;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class ArquiveiApiServiceTest extends TestCase
{
    protected ArquiveiApiService $arquivei;

    protected function setUp(): void
    {
        parent::setUp();

        $this->arquivei = new ArquiveiApiService();
    }

    public function test_returns_a_collection_of_nfes_dto()
    {
        Http::fake(["https://sandbox-api.arquivei.com.br/v1/nfe/received" => [
            'data' => [
                ['access_key' => '1234567', 'xml' => $this->getExampleNfeXml()]
            ]
        ]]);

        $response = $this->arquivei->getNfes();
        $expected = collect([new NfeResponse('1234567', 1348.00)]);

        $this->assertEquals($expected, $response);
    }

    public function test_paginates_if_page_property_is_defined()
    {
        Http::fake(["https://sandbox-api.arquivei.com.br/v1/nfe/received" =>
            Http::sequence()
                ->push([
                    'data' => [
                        ['access_key' => '1234567', 'xml' => $this->getExampleNfeXml()]
                    ],
                    'page' => [
                        'next' => 'https://sandbox-api.arquivei.com.br/v1/nfe/received'
                    ]
                ])
            ->push([
                'data' => [
                    ['access_key' => '1234567', 'xml' => $this->getExampleNfeXml()]
                ],
            ])
        ]);

        $response = $this->arquivei->getNfes();
        $expected = collect([new NfeResponse('1234567', 1348.00), new NfeResponse('1234567', 1348.00)]);

        $this->assertEquals($expected, $response);
    }

    public function test_returns_empty_collection_when_encountering_error()
    {
        Http::fake([
            'https://sandbox-api.arquivei.com.br/v1/nfe/received' => Http::response('Error', 400),
        ]);

        $response = $this->arquivei->getNfes();
        $expected = collect();

        $this->assertEquals($expected, $response);
    }

    private function getExampleNfeXml(): string
    {
        return base64_encode('<nfeProc><NFe><infNFe><total><ICMSTot><vNF>1348.00</vNF></ICMSTot></total></infNFe></NFe></nfeProc>');
    }
}
