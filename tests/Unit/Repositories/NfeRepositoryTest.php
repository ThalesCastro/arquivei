<?php

namespace Repositories;

use App\DataTransferObjects\NfeResponse;
use App\Repositories\NfeRepositoryInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NfeRepositoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_insert_nfe_response_collection()
    {
        /** @var NfeRepositoryInterface $nfeRepository */
        $nfeRepository = app(NfeRepositoryInterface::class);

        $nfeResponseCollection = collect([
            new NfeResponse('1', 123),
            new NfeResponse('2', 7456),
        ]);

        $nfeRepository->insertNfeResponseCollection($nfeResponseCollection);

        $this->assertDatabaseCount('nfes', 2);
        $this->assertDatabaseHas('nfes', ['id' => '1']);
        $this->assertDatabaseHas('nfes', ['id' => '2']);
    }
}

