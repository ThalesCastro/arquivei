<?php

namespace App\Console\Commands;

use App\Repositories\NfeRepositoryInterface;
use App\Services\ArquiveiApiService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class LoadNfes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nfe:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load all NFEs returned by the API into the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ArquiveiApiService $arquivei
     * @param NfeRepositoryInterface $nfeRepository
     * @return int
     */
    public function handle(ArquiveiApiService $arquivei, NfeRepositoryInterface $nfeRepository): int
    {
        $this->info("Making API call to Arquivei...");
        $nfes = $this->getNfeFromApi($arquivei);
        $this->info("done");

        $this->info("Inserting {$nfes->count()} NFes to the database...");
        $nfeRepository->insertNfeResponseCollection($nfes);
        $this->info("done");

        return 0;
    }

    /**
     * @param ArquiveiApiService $arquivei
     * @return Collection
     */
    private function getNfeFromApi(ArquiveiApiService $arquivei): Collection
    {
        try {
           return $arquivei->getNfes();
        } catch (Exception $exception) {
            $this->error("An error occurred when trying to get NFEs from the API:");
            $this->error($exception->getMessage());

            exit;
        }
    }

}
