<?php

namespace App\Http\Controllers;

use App\Models\Nfe;
use App\Repositories\NfeRepositoryInterface;
use Illuminate\Http\JsonResponse;

class NfeController extends Controller
{
    private NfeRepositoryInterface $nfeRepository;

    public function __construct(NfeRepositoryInterface $nfeRepository)
    {
        $this->nfeRepository = $nfeRepository;
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $nfe = $this->nfeRepository->find($id);

        if (! $nfe) {
            return response()->json(['error' => "NFe with access_key '{$id}' not found"], 404);
        }

        return response()->json(['value' => $nfe->value]);
    }
}
