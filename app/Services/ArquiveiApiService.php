<?php


namespace App\Services;

use App\DataTransferObjects\NfeResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class ArquiveiApiService
{
    private string $baseUrl;

    public function __construct(string $version = 'v1')
    {
        $this->baseUrl = "https://sandbox-api.arquivei.com.br/$version/";
    }

    /**
     * Makes the API call to Arquivei backend and returns the collection of
     * objects used on the application
     *
     * @return Collection<NfeResponse>
     */
    public function getNfes(): Collection
    {
        $allNfes = collect();
        $url = $this->baseUrl . "nfe/received";

        do {
            $response = Http::withHeaders([
                    'Content-Type' => 'application/json',
                    'X-API-ID' => '329ea218aa65778fad452643fe4d9bdeba0673e6',
                    'X-API-KEY' => '39020d7f2ff4485632166f578d486f0ab74174e0'
                ])
                ->get($url);

            $body = json_decode($response->body());

            $nfes = $body->data ?? [];
            $url = $body?->page?->next ?? null;

            $allNfes->push(...$nfes);
        } while (count($nfes) && $url);

        return $this->createDtoCollection($allNfes);
    }

    /**
     * Given the API Response, returns the collection of objects
     * @param Collection $nfes
     * @return Collection<NfeResponse>
     */
    private function createDtoCollection(Collection $nfes): Collection
    {
        $nfeResponseCollection = collect();

        foreach ($nfes as $nfe) {
            $accessKey = $nfe->access_key;
            $value = $this->getNfeValue($nfe->xml);

            $nfeResponseCollection->push(new NfeResponse($accessKey, $value));
        }

        return  $nfeResponseCollection;
    }

    /**
     *
     * @param string $nfe
     * @return float
     */
    private function getNfeValue(string $nfe): float
    {
        $decodedNfeXml = base64_decode($nfe);
        $nfeXmlElement = simplexml_load_string($decodedNfeXml);

        return (float) $nfeXmlElement?->NFe?->infNFe?->total?->ICMSTot?->vNF;
    }
}
