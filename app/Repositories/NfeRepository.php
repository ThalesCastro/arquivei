<?php


namespace App\Repositories;


use App\DataTransferObjects\NfeResponse;
use App\Models\Nfe;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class NfeRepository implements NfeRepositoryInterface
{
    public function find(string $id): ?Nfe
    {
        return Nfe::find($id);
    }

    /**
     * @param Collection<NfeResponse> $nfes
     */
    public function insertNfeResponseCollection(Collection $nfes): void
    {
        $now = Carbon::now();

        $nfesArray = $nfes
            ->map(function (NfeResponse $nfe) use ($now) {
                return ['id' => $nfe->id, 'value' => $nfe->value, 'created_at' => $now, 'updated_at' => $now];
            })
            ->toArray();

        Nfe::insertOrIgnore($nfesArray);
    }
}
