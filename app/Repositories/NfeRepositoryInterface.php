<?php


namespace App\Repositories;


use App\DataTransferObjects\NfeResponse;
use App\Models\Nfe;
use Illuminate\Support\Collection;

interface NfeRepositoryInterface
{
    /**
     * @param string $id
     * @return Nfe|null
     */
    public function find(string $id): ?Nfe;

    /**
     * @param Collection<NfeResponse> $nfes
     */
    public function insertNfeResponseCollection(Collection $nfes): void;
}
