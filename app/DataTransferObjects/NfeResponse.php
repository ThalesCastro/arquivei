<?php


namespace App\DataTransferObjects;


class NfeResponse
{
    public string $id;
    public float $value;

    /**
     * NfeResponse constructor.
     * @param string $accessKey
     * @param float $value
     */
    public function __construct(string $accessKey, float $value)
    {
        $this->id = $accessKey;
        $this->value = $value;
    }
}
