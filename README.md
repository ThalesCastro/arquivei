## Projeto Arquivei

O projeto foi realizado utilizando o [Laravel 8](https://laravel.com/) para a aplicação e com o
[MySQL 8.0.21](https://www.mysql.com/) para o banco (sendo o [SQLite](https://www.sqlite.org/index.html)
usado nos testes). A versão do [PHP](https://www.php.net/) foi a 8 para aproveitar as novas funcionalidades
como o *null safe operator*, *null coalescing operator* e propriedades tipadas. Esses dois
últimos já disponíveis na versão 7.4.

Este documento está dividido da seguinte forma:

1. [Rodando o projeto no Docker](#markdown-header-1-rodando-o-projeto-no-docker)
2. [Organização do projeto](#markdown-header-2-organizacao-do-projeto)
    - [ArquiveiApiService](#markdown-header-21-arquiveiapiservice)
        - [NfeResponse](#markdown-header-211-nferesponse)
    - [NfeRepository](#markdown-header-22-nferepository)
    - [LoadNfes](#markdown-header-23-loadnfes)
    - [NfeController](#markdown-header-23-nfecontroller)

### 1: **Rodando o projeto no Docker**
Com o objetivo de acelerar o setup do docker, utilizei um projeto mantido pela comunidade 
chamado [Laradock](https://github.com/laradock/laradock). Ele possui um ambiente pré-configurado
com tudo necessário para um projeto em Laravel.

Mantive a maior parte das configurações como a padrão. Fiz apenas algumas mudanças nas portas
para evitar conflitos com possíveis programas que podem estar rodando na sua máquina. Essas
chaves estão no arquivo `.env` da pasta `laradock`, que será criado na próxima etapa:

- Defini a porta `nginx` como `82` (`NGINX_HOST_HTTP_PORT=82`)
- Defini a porta do `mysql` como `3308` (`MYSQL_PORT=3308`)

Agora vamos aos passos para rodar e testar o projeto:

1. clone este repositorio e entre na pasta
3. `cp .env.example .env`
4. `cd laradock`
5. `cp .env.example .env`
6. `sudo docker-compose up -d nginx mysql` - esse pode demorar um pouco na primeira vez
7. `sudo  docker-compose exec workspace bash` para entrar no bash do container
8. `composer install && php artisan key:generate && php artisan migrate` para deixar tudo preparado
10. `php artisan test` para ver o resultado dos testes
9. Não feche este bash ainda

Para conferir que o banco não tem nenhuma NFe ainda, execute os seguintes comandos:

1. abra outro terminal na mesma pasta `laradock` dentro do projeto
2. `sudo docker-compose exec mysql bash` para abrir o bash do container onde está o banco
3. `mysql -u root -p` em que a senha também é `root`
4. `use local;` para selecionar a database onde está a tabela
5. `SELECT * FROM nfes;` para ver que não há nenhuma linha no banco
6. Também deixe esse aberto por enquanto
   
Por último, vamos rodar o comando que de fato insere as notas

1. No bash do container anterior, o `workspace`, execute `php artisan nfe:load`
2. Depois que o comando terminar, volte no bash do `mysql` e execute 
   `SELECT * FROM nfes;` novamente para ver o banco populado
      
Por fim, para conferir que o endpoint REST retornando o valor de uma nota, o endpoint é `api/nfe/{access_key}`.
Um exemplo seria [http://localhost:82/api/nfe/17171154759614000180550010000282821000324478](http://localhost:82/api/nfe/17171154759614000180550010000282821000324478),
que deve retornar um JSON no formato: `{"value":129.02}`.

### 2: **Organização do projeto**
Agora entrando na questão do código. Para fazer a primeira parte do desafio, que envolveu
a chamada à API e o salvamento dos dados no banco, criei um comando do console chamado `nfe:load`.
Esse comando possui duas dependencias: `ArquiveiApiService` e `NfeRepositoryInterface`.

#### 2.1: **ArquiveiApiService** <a href="arquivei-api-service"></a>
Esse é o serviço que encapsula todo a comunicação com a API e o tratamento da sua resposta.
Possui um método público chamado `getNfes`. Esse método é responsável pelas requisições HTTP,
e invocar o método que fará a `Collection` com o `DataTransferObject` `NfeResponse` que será
usado no resto do programa. Dessa forma, ela é a única classe que possui conhecimento da API e 
do XML retornado.

#### 2.1.1 **NfeResponse** <a href="nfe-response"></a>
Essa é apenas uma classe para organizar a forma que os dados relevantes são passados 
pelo programa. Possui duas propriedades públicas: `id` e `value`, que vai ser todo o necessário
pelo resto do desafio. Nesse caso, o `id` é a `access_key` daquela NFe;

### 2.2 **NfeRepository** <a href="nfe-repository"></a>
Para encapsular a comunicação com o banco e facilitar possíveis mudanças, essa classe possui 
os métodos `find` e `insertNfeResponseCollection`. Esse último recebe uma `Collection` de 
`NfeResponse` e os insere na tabela `nfes`.

Com o intuito facilitar na hora de fazer a query posteriormente, o próprio `access_key` retornado no XML
é usado como chave primária da tabela.

### 2.3 **LoadNfes** <a href="load-nfes"></a>
Por fim, para juntar o fluxo desde a chamada à API até a inserção no banco de dados, criei o
comando `php artisan nfe:load`. O seu trabalho foi apenas usar os serviços acima e pode ser resumido
da seguinte forma:

```php
 public function handle(ArquiveiApiService $arquivei, NfeRepositoryInterface $nfeRepository)
 {
        $nfes = $arquivei->getNfes();

        $nfeRepository->insertNfeResponseCollection($nfes);
 }
```

### 2.3 **NfeController** <a href="nfe-controller"></a>
Após as notas estarem no banco, fazer o endpoint pra buscar o valor da nota no banco 
foi simples. O `NfeController` possui apenas o método `show` que é invocado 
fazendo um `GET` para a rota `api/nfe/{access_key}`.
